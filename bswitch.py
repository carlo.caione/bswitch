#!/usr/bin/env python3

import argparse
import contextlib
import io
import glob
import shutil
import oyaml as yaml
import os
import sys
import subprocess
import textwrap

from bswitch_def import *

PATH_BS_BASE = "~/proj/bswitch/base/"


def check_mandatory(sec, mandatory):
    missing = mandatory - set(sec.keys())
    if len(missing) > 0:
        raise Exception("Missing: " + str(missing))


class Conf():
    def __init__(self, board, yaml_conf, bs_conf):
        self.yaml = yaml_conf

        self.merge_diff(board.yaml, self.yaml)
        check_mandatory(self.yaml, MANDATORY_CONF)
        check_mandatory(self.get_k(K_TFTP), MANDATORY_TFTP)

        self.board = board
        self.name = self.get_k(K_NAME)
        self.dumper = bs_conf[KBS_DUMPER]
        self.bs_conf = bs_conf

    def merge_diff(self, master, slave):
        master_k = set(master.keys()) - K_EXCL_BOARD
        slave_k = set(slave.keys()) - K_EXCL_BOARD

        diff_k = master_k - slave_k
        for k in diff_k:
            slave[k] = master[k]

        eq_k = master_k & slave_k
        for k in eq_k:
            if isinstance(master[k], dict):
                self.merge_diff(master[k], slave[k])

    def get_k(self, k):
        if k in self.yaml:
            return self.yaml[k]

        if k in self.yaml[K_TFTP]:
            return self.yaml[K_TFTP][k]

        return None

    def get_script(self):
        initrd_addr = self.get_k(K_INITRD)

        script = ""
        initrd_placeholder = "-"

        if initrd_addr:
            initrd_placeholder = hex(initrd_addr)
            script += TEMPLATE_TFTP_SRC_INITRD.format(initrd_addr=initrd_placeholder)

        script += TEMPLATE_TFTP_SRC.format(bootargs=self.get_k(K_BOOTARGS),
                                           kernel_addr=self.get_k(K_KERNEL),
                                           dtb_addr=self.get_k(K_DTB),
                                           initrd_addr=initrd_placeholder)
        return script

    def __str__(self):
        return yaml.dump(self.yaml, default_flow_style=False, Dumper=self.dumper)


class Board():
    def __init__(self, yaml_board, bs_conf):
        self.yaml = yaml_board[K_BOARD]

        check_mandatory(self.yaml, MANDATORY_BOARD)

        self.conf = []
        self.name = self.get_k(K_NAME)
        self.dumper = bs_conf[KBS_DUMPER]
        self.bs_conf = bs_conf

        for yaml_conf in self.get_k(K_CONF):
            conf = Conf(self, yaml_conf, bs_conf)
            self.conf.append(conf)

        for k in set(self.yaml.keys()) - K_IN_BOARD:
            del(self.yaml[k])

    def get_k(self, k):
        return self.yaml[k] if k in self.yaml else None

    def full_print(self):
        print(self)
        for c in self.conf:
            print(textwrap.indent(str(c), " " * 2))

    def get_bootcmd(self):
        k_hwaddr = self.get_k(K_HWADDR)
        k_ipaddr = self.get_k(K_IPADDR)
        k_script = self.get_k(K_SCRIPT)

        bootcmd = ""
        if k_hwaddr:
            bootcmd += TEMPLATE_BOOTCMD_HWADDR.format(hwaddr=k_hwaddr)
        if k_ipaddr:
            bootcmd += TEMPLATE_BOOTCMD_IPADDR.format(ipaddr=k_ipaddr)

        bootcmd += TEMPLATE_BOOTCMD.format(serverip=self.bs_conf[KBS_SERVERIP],
                                           tftp_addr=k_script,
                                           uboot_file=FILE_TFTP_UBOOT)
        return bootcmd

    def __str__(self):
        return yaml.dump(self.yaml, default_flow_style=False, Dumper=self.dumper)


def load_board(bs, bs_conf):
        with open(bs, "r") as yaml_board:
            try:
                yaml_board = yaml.load(yaml_board)
                board = Board(yaml_board, bs_conf)
                board.bs = bs
            except Exception as err:
                print(f"Error in {bs}")
                print(err)
                sys.exit(1)
            else:
                return board


def get_boards_lut(bs_conf):
    lut = { }

    for bs in glob.glob(os.path.join(bs_conf[KBS_BS_DIR], "*.bs")):
        b = load_board(bs, bs_conf)

        lut[b.name] = { }
        lut[b.name][K_BOARD] = b

        for c in b.conf:
            lut[b.name][c.name] = c

    return lut


def get_active(lut, bs_conf):
    back_link = os.path.join(bs_conf[KBS_TFTP_DIR], DIR_ACTIVE)

    if not os.path.islink(back_link):
        return (None, None)

    f_name = os.readlink(back_link)
    (head, conf) = os.path.split(f_name)
    board = os.path.basename(head)

    b = lut[board][K_BOARD]
    c = lut[board][conf]

    return (b, c)


def print_overview(lut, b_act=None, c_act=None):
    print("\nBoards and configurations available:\n")
    for b_name in lut.keys():
        b = lut[b_name][K_BOARD]
        group = b.get_k(K_GROUP)
        b_desc = b.get_k(K_DESCRIPTION)

        print(f" [{b_name}] ({group}) - {b_desc}" if group else f"[{b_name}] {b_desc}")
        for c in lut[b_name][K_BOARD].conf:
            c_desc = c.get_k(K_DESCRIPTION)

            act = "    [active]" if (b == b_act and c == c_act) else ""
            print(f" |- [{c.name}] - {c_desc} {act}")
        print()


def do_list(args, lut, bs_conf):
    if args.board != "all":
        (b, _) = get_board_and_conf(lut, args.board)
        b.full_print()
        return

    if args.group != "all":
        for board in lut:
            b = lut[board][K_BOARD]

            if args.group != b.get_k(K_GROUP):
                continue

            print(80 * "-")
            b.full_print()
        return

    (b_act, c_act) = get_active(lut, bs_conf)
    print_overview(lut, b_act, c_act)


def exist(lut, board, conf=None):
    if board not in lut:
        return False

    if not conf:
        return True

    if conf not in lut[board]:
        return False

    return True


def get_board_and_conf(lut, board, conf=None):
    if not exist(lut, board, conf):
        print("board and configuration not found.")
        print_overview(lut)
        sys.exit(1)

    b = lut[board][K_BOARD]
    c = None

    if conf:
        c = lut[board][conf]

    return (b, c)


def get_dir(board, conf=None):
    path_board = os.path.join(board.bs_conf[KBS_BS_DIR], board.name)
    if not os.path.exists(path_board):
        os.makedirs(path_board)

    path_conf = None
    if conf:
        path_conf = os.path.join(path_board, conf.name)
        if not os.path.exists(path_conf):
            os.makedirs(path_conf)

    return (path_board, path_conf)


def do_bootcmd(args, lut, bs_conf):
    (b, _) = get_board_and_conf(lut, args.board)
    (board_dir, _) = get_dir(b)

    bootcmd = b.get_bootcmd()
    bootcmd_src = os.path.join(board_dir, FILE_BOOTCMD)

    try:
        with open(bootcmd_src, "w") as f_bootcmd_src:
            f_bootcmd_src.write(bootcmd)
    except Exception as err:
        print(err)
        sys.exit(1)

    print("\n" + textwrap.indent(bootcmd, " " * 2))
    print(f"Saved to: {bootcmd_src}")

def do_activate(args, lut, bs_conf):
    (b, c) = get_board_and_conf(lut, args.board, args.configuration)
    (_, conf_dir) = get_dir(b, c)

    script = c.get_script()
    tftp_src = os.path.join(conf_dir, FILE_TFTP_SCRIPT)

    try:
        with open(tftp_src, "w") as f_tftp_src:
            f_tftp_src.write(script)
    except Exception as err:
        print(err)
        sys.exit(1)

    print("\n" + textwrap.indent(script, " " * 2))
    print(f"Saved to: {tftp_src}\n")

    uboot_script = os.path.join(conf_dir, FILE_TFTP_UBOOT)
    mk_uboot_script = TEMPLATE_TFTP_UBOOT.format(bname=b.name,
                                                 cname=c.name,
                                                 src_file=tftp_src,
                                                 uboot_file=uboot_script)

    mk_out = subprocess.check_output(mk_uboot_script, shell=True, universal_newlines=True)
    print(textwrap.indent(mk_out, " " * 2))
    print(f"Generated: {uboot_script }")

    back_link = os.path.join(bs_conf[KBS_TFTP_DIR], DIR_ACTIVE)

    with contextlib.suppress(FileNotFoundError):
        os.remove(back_link)

    try:
        os.symlink( conf_dir, back_link)
    except Exception as err:
        print("Fail during backlinking: " + str(err))
        sys.exit(1)


    print("\nHardlinking...\n")
    for param in PARAM_ACT:
        p_bs = param + ".bs"
        f_link_conf = os.path.join(conf_dir, p_bs)
        f_link_dest = os.path.join(bs_conf[KBS_TFTP_DIR], p_bs)

        if os.path.exists(f_link_conf):

            if os.path.islink(f_link_conf):
                f_name = os.readlink(f_link_conf)
                f_link_src = os.path.join(conf_dir, f_name)
            else:
                f_link_src = f_link_conf

            with contextlib.suppress(FileNotFoundError):
                os.remove(f_link_dest)

            try:
                os.link(f_link_src, f_link_dest)
            except Exception as err:
                print("Fail during activation: " + str(err))
                sys.exit(1)

            print(f"[{b.name}:{c.name}] {f_link_dest} --> {f_link_src}")


def do_feed(args, lut, bs_conf):
    (b, c) = get_board_and_conf(lut, args.board, args.configuration)
    (_, conf_dir) = get_dir(b, c)

    for param in PARAM_FEED:
        f = getattr(args, param)
        if not f:
            continue

        f_src = os.path.abspath(f)
        f_name = os.path.basename(f_src)
        f_link = os.path.join(conf_dir, param + ".bs")

        if os.path.islink(f_link):
            f_link_dest = os.readlink(f_link)
            f_old = os.path.join(conf_dir, f_link_dest)
            os.remove(f_old)
            os.remove(f_link)

        try:
            shutil.copy(f_src, conf_dir)
            os.symlink(f_name, f_link)
        except Exception as err:
            print(err)
            sys.exit(1)

        print(f"[{b.name}:{c.name}] {f_link} --> {f_name}")

    (b_act, c_act) = get_active(lut, bs_conf)
    if (b == b_act and c == c_act):
        do_activate(args, lut, bs_conf)


def parse_cmd(lut, bs_conf):
    parser = argparse.ArgumentParser(description='Board Switching Helper')
    subparser = parser.add_subparsers(help='bswitch commands')

    parser_list = subparser.add_parser('list')
    parser_list.set_defaults(func=do_list)
    parser_list.add_argument('-b', help='show board', dest='board', default='all')
    parser_list.add_argument('-g', help='show group', dest='group', default='all')

    parser_bootcmd = subparser.add_parser('bootcmd')
    parser_bootcmd.set_defaults(func=do_bootcmd)
    parser_bootcmd.add_argument('board')

    parser_activate = subparser.add_parser('activate')
    parser_activate.set_defaults(func=do_activate)
    parser_activate.add_argument('board')
    parser_activate.add_argument('configuration')

    parser_feed = subparser.add_parser('feed')
    parser_feed.set_defaults(func=do_feed)
    parser_feed.add_argument('board')
    parser_feed.add_argument('configuration')
    parser_feed.add_argument('-k', help='add kernel', dest='kernel')
    parser_feed.add_argument('-i', help='add initrd', dest='initrd')
    parser_feed.add_argument('-d', help='add dtb', dest='dtb')

    args = parser.parse_args()

    if len(sys.argv) < 2:
        parser.print_usage()
    else:
        args.func(args, lut, bs_conf)


def hexint_presenter(dumper, data):
    return dumper.represent_int(hex(data))


def setup_dumper(bs_conf):
    noalias_dumper = yaml.dumper.SafeDumper
    noalias_dumper.ignore_aliases = lambda self, data: True
    noalias_dumper.add_representer(int, hexint_presenter)

    bs_conf[KBS_DUMPER] = noalias_dumper


def get_bs_conf():
    file_bs_conf = os.path.join(os.path.expanduser(PATH_BS_BASE), FILE_BS_CONF)

    try:
        with open(file_bs_conf, "r") as file_bs_conf:
            yaml_bs_conf = yaml.load(file_bs_conf)
    except Exception as err:
        print(err)
        sys.exit(1)

    if KBS_BSWITCH not in yaml_bs_conf:
        print(f"Missing {KBS_BSWITCH} section in {file_bs_conf.name}")
        sys.exit(1)

    bs_conf = yaml_bs_conf[KBS_BSWITCH]

    if KBS_TFTP_DIR not in bs_conf:
        print(f"Missing {KBS_TFTP_DIR} entry in {file_bs_conf.name}")
        sys.exit(1)

    if KBS_BS_DIR not in bs_conf:
        path_boards = os.path.join(os.path.expanduser(PATH_BS_BASE), "boards")
        bs_conf[KBS_BS_DIR] = path_boards

    return bs_conf


if __name__ == '__main__':
    bs_conf = get_bs_conf()
    setup_dumper(bs_conf)

    lut = get_boards_lut(bs_conf)

    parse_cmd(lut, bs_conf)
