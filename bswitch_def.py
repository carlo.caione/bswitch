FILE_BS_CONF =  "bswitch.bs"

K_BOARD =       "board"
K_BOOTARGS =    "bootargs"
K_BS_FILE =     "bs_file"
K_CONF =        "configuration"
K_DESCRIPTION = "description"
K_DTB =         "dtb"
K_GROUP =       "group"
K_HWADDR =      "hwaddr"
K_INITRD =      "initrd"
K_IPADDR =      "ipaddr"
K_KERNEL =      "kernel"
K_NAME =        "name"
K_SCRIPT =      "script_addr"
K_TFTP =        "tftp_address"

KBS_BSWITCH =   "bswitch"
KBS_SERVERIP =  "serverip"
KBS_TFTP_DIR =  "tftp_dir"
KBS_BS_DIR =    "boards_dir"
KBS_DUMPER =    "dumper"

K_IN_BOARD =    {K_NAME, K_DESCRIPTION, K_GROUP, K_HWADDR, K_IPADDR, K_SCRIPT}
K_EXCL_BOARD =  {K_NAME, K_DESCRIPTION, K_GROUP, K_HWADDR, K_IPADDR, K_SCRIPT, K_CONF}

MANDATORY_BOARD =   {K_NAME, K_SCRIPT, K_CONF}
MANDATORY_CONF =    {K_BOOTARGS, K_NAME, K_TFTP}
MANDATORY_TFTP =    {K_DTB, K_KERNEL}

PARAM_FEED =    {"kernel", "dtb", "initrd"}
PARAM_ACT =     {"kernel", "dtb", "initrd", "tftp-script.u-boot"}

DIR_ACTIVE =       "active.bs"

FILE_BOOTCMD =      "bootcmd.src"
FILE_TFTP_SCRIPT =  "tftp-script.src"
FILE_TFTP_UBOOT =   "tftp-script.u-boot.bs"

TEMPLATE_BOOTCMD_HWADDR =   "setenv hwaddr {hwaddr}\n"
TEMPLATE_BOOTCMD_IPADDR =   "setenv ipaddr {ipaddr}\n"
TEMPLATE_BOOTCMD =          "setenv serverip {serverip}\n" \
                            "setenv autoload no\n" \
                            "setenv bootcmd '" \
                            "if test -z \"$ipaddr\"; then dhcp; fi; " \
                            "tftp {tftp_addr:#x} {uboot_file}; " \
                            "source {tftp_addr:#x}'\n" \
                            "saveenv\n"

TEMPLATE_TFTP_SRC_INITRD =  "tftp {initrd_addr} initrd.bs\n"
TEMPLATE_TFTP_SRC =         "setenv bootargs '{bootargs}'\n" \
                            "tftp {kernel_addr:#x} kernel.bs\n" \
                            "tftp {dtb_addr:#x} dtb.bs\n" \
                            "booti {kernel_addr:#x} {initrd_addr} {dtb_addr:#x}\n"

TEMPLATE_TFTP_UBOOT =       "mkimage -A arm -O linux -T script -C none -n '{bname}:{cname}' -d {src_file} {uboot_file}"


