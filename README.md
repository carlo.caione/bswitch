# Dependencies and requirements
`oyaml` - https://github.com/wimglenn/oyaml

`u-boot-tools` - to be able to use `mkimage`

`bswitch` need a local TFTP server (i.e. `tftpd-hpa`) running on your machine.

# Use case (1)
We are working at the same time on two different boards: an Apollo board with
an Amlogic AXG SoC and an i.MX8MQ evaluation board.

For each board we have a downstream and an upstream kernel and during
development we need to switch from one to the other. We also have an active
development branch where we are testing new changes to the DTB on the Amlogic
board.

## Boards setup
Both the boards are loading kernel and DTB (and possibly initrd) from a TFTP
server.

## Introducing bswitch

### bswitch configuration
Create a new base directory i.e. `~/proj/bswitch/base/` and a new board
directory i.e. `~/proj/bswitch/base/boards` to hold all the boards related
files.
In the `bswitch.py` script make the `PATH_BS_BASE` variable pointing to the
correct base location.

In the base directory `~/proj/bswitch/base/` create the configuration file
`bswitch.bs`. For example:
```
bswitch:
    serverip: 192.168.1.1
    tftp_dir: /var/lib/tftpboot
```
Where `serverip` is the IP of the tftp server (local IP of the machine) and
`tftp_dir` is the tftp directory.

### boargs configuration
In the board directory `~/proj/bswitch/base/boards` creates the two board files
for the boards.

`apollo.bs` for the Amlogic board:
```
board:
    name: apollo
    description: apollo board
    group: Amlogic
    hwaddr: aa:bb:cc:aa:bb:cc
    ipaddr: 192.168.1.2
    script_addr: 0x08000000

    bootargs: earlycon=meson,0xff803000 console=ttyAML0,115200 root=/dev/mmcblk0p1 rw rootwait
    
    tftp_address:
        dtb: 0x08008000
        kernel: 0x08080000

    configuration:
        - name: downstream
          description: downstream kernel

        - name: upstream
          description: upstream kernel booting from SD
          bootargs: earlycon=meson,0xff803000 console=ttyAML0,115200 root=/dev/mmcblk1p1 rw rootwait

        - name: experimental
          description: downstream kernel with new DTB
```
And `imx8mq.bs` for the i.MX board:
```
board:
    name: i.MX8MQ
    description: i.MX8MQ evaluation board
    group: imx
    hwaddr: aa:bb:cc:aa:bb:cc
    ipaddr: 192.168.1.2
    script_addr: 0x40480000

    bootargs: console=ttymxc0,115200 earlycon=ec_imx6q,0x30860000,115200 root=/dev/mmcblk0p1 rootwait rw

    tftp_address:
        dtb: 0x48000000
        kernel: 0x43000000

    configuration:
        - name: upstream
          description: upstream kernel with a different address for the kernel
          tftp_address:
              kernel: 0x42000000
```
Each configuration can overwrite the generic board settings.

### Exploring boards and configurations
We can check boards and configurations using the `bswitch list` command:
```
$ bswitch.py list
Boards and configurations available:

[apollo] (Amlogic)
|- downstream
|- upstream
|- experimental

[i.MX8MQ] (imx)
|- upstream
```
And inspect a single board with the `-b <board>` parameter:
```
$ bswitch.py list -b apollo
name: apollo
description: apollo board
group: Amlogic
hwaddr: aa:bb:cc:aa:bb:cc
ipaddr: 192.168.1.2
script_addr: 0x8000000

  name: downstream
  description: downstream kernel
  bootargs: earlycon=meson,0xff803000 console=ttyAML0,115200 root=/dev/mmcblk0p1 rw
    rootwait
  tftp_address:
    dtb: 0x8008000
    kernel: 0x8080000

  name: upstream
  description: upstream kernel booting from SD
  bootargs: earlycon=meson,0xff803000 console=ttyAML0,115200 root=/dev/mmcblk1p1 rw
    rootwait
  tftp_address:
    dtb: 0x8008000
    kernel: 0x8080000

  name: experimental
  description: downstream kernel with new DTB
  bootargs: earlycon=meson,0xff803000 console=ttyAML0,115200 root=/dev/mmcblk0p1 rw
    rootwait
  tftp_address:
    dtb: 0x8008000
    kernel: 0x8080000
```
Or even a single group using the `-g <group>` parameter.

### U-Boot board setup
At this point we can generate for each board the `bootcmd` script ready to be
copy-and-pasted into U-Boot.

For example for the apollo board:
```
$ bswitch.py bootcmd apollo

  setenv hwaddr aa:bb:cc:aa:bb:cc
  setenv ipaddr 192.168.1.2
  setenv serverip 192.168.1.1
  setenv autoload no
  setenv bootcmd 'if test -z "$ipaddr"; then dhcp; fi; tftp 0x8000000 tftp-script.u-boot.bs; source 0x8000000'
  saveenv

Saved to: /home/carlo/proj/bswitch/base/boards/apollo/bootcmd.src
```

### Feed kernel, DTB and initrd
We need to feed kernel, DTB and (optional) initrd for each configuration we
want to use.

For example for the `apollo` board on the `downstream` configuration: 
```
$ bswitch.py feed apollo downstream -k arch/arm64/boot/Image -d arch/arm64/boot/dts/amlogic/meson-axg-apollo.dtb
[apollo:downstream] meson-axg-apollo.dtb --> /home/carlo/proj/bswitch/base/boards/apollo/downstream/dtb.bs
[apollo:downstream] Image --> /home/carlo/proj/bswitch/base/boards/apollo/downstream/kernel.bs
```
Each board/configuration can have different kernel, DTBs and initrd images.

### Activate board and configuration
Once we are done feeding the boards and configurations we can activate the
configuration we want to boot.

For example if we decide we want to boot the downstream configuration for the
apollo board:
```
$ bswitch.py activate apollo downstream

  setenv bootargs 'earlycon=meson,0xff803000 console=ttyAML0,115200 root=/dev/mmcblk0p1 rw rootwait'
  tftp 0x8080000 kernel.bs
  tftp 0x8008000 dtb.bs
  booti 0x8080000 - 0x8008000

Saved to: /home/carlo/proj/bswitch/base/boards/apollo/downstream/tftp-script.src

  Image Name:   apollo:downstream
  Created:      Sun Feb 10 17:00:13 2019
  Image Type:   ARM Linux Script (uncompressed)
  Data Size:    182 Bytes = 0.18 KiB = 0.00 MiB
  Load Address: 00000000
  Entry Point:  00000000
  Contents:
     Image 0: 174 Bytes = 0.17 KiB = 0.00 MiB

Generated: /home/carlo/proj/bswitch/base/boards/apollo/downstream/tftp-script.u-boot.bs

Hardlinking...

[apollo:downstream] /var/lib/tftpboot/tftp-script.u-boot.bs --> /home/carlo/proj/bswitch/base/boards/apollo/downstream/tftp-script.u-boot.bs
[apollo:downstream] /var/lib/tftpboot/dtb.bs --> /home/carlo/proj/bswitch/base/boards/apollo/downstream/meson-axg-apollo.dtb
[apollo:downstream] /var/lib/tftpboot/kernel.bs --> /home/carlo/proj/bswitch/base/boards/apollo/downstream/Image           
```
Now we can just power-on the board and that's it.

Changing board / configuration is as easy as just giving a new `bswitch activate` command.
